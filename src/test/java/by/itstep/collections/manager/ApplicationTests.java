package by.itstep.collections.manager;

import by.itstep.collections.manager.entity.Collection;
import by.itstep.collections.manager.entity.CollectionItem;
import by.itstep.collections.manager.repository.CollectionItemRepository;
import by.itstep.collections.manager.repository.CollectionItemRepositoryImpl;
import by.itstep.collections.manager.repository.CollectionRepository;
import by.itstep.collections.manager.repository.CollectionRepositoryImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.List;

@SpringBootTest
class ApplicationTests {

	private CollectionRepository collectionRepository = new CollectionRepositoryImpl();
	private CollectionItemRepository itemRepository = new CollectionItemRepositoryImpl();

	@BeforeEach
	void setUp() {
		itemRepository.deleteAll();
		collectionRepository.deleteAll();
	}

	@Test
	void testCreate() {
		// given
		Collection collection = Collection.builder()
				.name("my-name")
				.description("my-descrition")
				.imageUrl("my-image")
				.title("my-title")
				.build();

		// when
		Collection saved = collectionRepository.create(collection);

		// then
		Assertions.assertNotNull(saved.getId());
	}

	@Test
	void findAll() {
		// given
		Collection collection = Collection.builder()
				.name("my-name")
				.description("my-descrition")
				.imageUrl("my-image")
				.title("my-title")
				.build();

		Collection collection2 = Collection.builder()
				.name("my-name2")
				.description("my-descrition2")
				.imageUrl("my-image2")
				.title("my-title2")
				.build();

		collectionRepository.create(collection);
		collectionRepository.create(collection2);

		// when
		List<Collection> list = collectionRepository.findAll();

		// then
		Assertions.assertEquals(2, list.size());


	}

	@Test
	void save_collectionWithoutItems() {
		//GIVEN
		Collection c = Collection.builder()
				.name("asd")
				.title("zxc")
				.imageUrl("qwe")
				.description("qwt")
				.build();

		//WHEN
		Collection saved = collectionRepository.create(c);

		//THEN
		Assertions.assertNotNull(saved.getId());
	}

	@Test
	void save_collectionWithItems() {
		//GIVEN
		Collection c = Collection.builder()
				.name("asd")
				.title("zxc")
				.imageUrl("qwe")
				.description("qwt")
				.build();

		Collection saved = collectionRepository.create(c);

		CollectionItem i1 = CollectionItem.builder()
				.name("item1")
				.collection(saved)
				.build();
		CollectionItem i2 = CollectionItem.builder()
				.name("item2")
				.collection(saved)
				.build();

		CollectionItem savedI1 = itemRepository.create(i1);
		CollectionItem savedI2 = itemRepository.create(i2);
		List<CollectionItem> items = new ArrayList<>();
		items.add(savedI1);
		items.add(savedI2);

		//THEN
		Assertions.assertNotNull(saved.getId());
	}

	@Test
	void findById_happyPath() {
		// given
		Collection c = Collection.builder()
				.name("asd")
				.title("zxc")
				.imageUrl("qwe")
				.description("qwt")
				.build();

		Collection saved = collectionRepository.create(c);

		CollectionItem i1 = CollectionItem.builder()
				.name("item1")
				.collection(saved)
				.build();
		CollectionItem i2 = CollectionItem.builder()
				.name("item2")
				.collection(saved)
				.build();

		itemRepository.create(i1);
		itemRepository.create(i2);

		// when
		Collection found = collectionRepository.findById(saved.getId());

		// then
		Assertions.assertNotNull(found);
		Assertions.assertNotNull(found.getId());
		Assertions.assertNotNull(found.getItems());
		Assertions.assertEquals(2, found.getItems().size());
	}

	@Test
	void test5() {

	}

}
