package by.itstep.collections.manager.service;

import by.itstep.collections.manager.dto.collectionDto.CollectionCreateDto;
import by.itstep.collections.manager.dto.collectionDto.CollectionFullDto;
import by.itstep.collections.manager.dto.collectionDto.CollectionPreviewDto;
import by.itstep.collections.manager.dto.collectionDto.CollectionUpdateDto;

import java.util.List;

public interface CollectionService {

    // 1. Найти все
    List<CollectionPreviewDto> findAll();

    // 2. Найти по ID
    CollectionFullDto findById(Long id);

    // 3. Сохранить новый
    CollectionFullDto create(CollectionCreateDto createDto);

    // 4. Обновить существующий
    CollectionFullDto update(CollectionUpdateDto collection);

    // 5. Удалить по ID
    void deleteById(Long id);

}
