package by.itstep.collections.manager.service;

import by.itstep.collections.manager.dto.collectionItemDto.CollectionItemCreateDto;
import by.itstep.collections.manager.dto.collectionItemDto.CollectionItemFullDto;
import by.itstep.collections.manager.dto.collectionItemDto.CollectionItemPreviewDto;
import by.itstep.collections.manager.dto.collectionItemDto.CollectionItemUpdateDto;
import by.itstep.collections.manager.entity.CollectionItem;
import by.itstep.collections.manager.mapper.CollectionItemMapper;
import by.itstep.collections.manager.repository.CollectionItemRepository;
import by.itstep.collections.manager.repository.CollectionItemRepositoryImpl;

import java.util.List;

public class CollectionItemServiceImpl implements CollectionItemService {

    private CollectionItemRepository collectionItemRepository = new CollectionItemRepositoryImpl();

    private CollectionItemMapper mapper = new CollectionItemMapper();

    @Override
    public List<CollectionItemPreviewDto> findAll() {
        List<CollectionItem> found = collectionItemRepository.findAll();

        return mapper.mapToDoList(found);
    }

    @Override
    public CollectionItemFullDto findById(Long id) {
        CollectionItem found = collectionItemRepository.findById(id);

        return mapper.mapToDto(found);
    }

    @Override
    public CollectionItemFullDto create(CollectionItemCreateDto createDto) {
        CollectionItem toSave = mapper.mapToEntity(createDto);

        CollectionItem created = collectionItemRepository.create(toSave);

        return mapper.mapToDto(created);
    }

    @Override
    public CollectionItemFullDto update(CollectionItemUpdateDto updateDto) {
        CollectionItem toUpdate = mapper.mapToEntity(updateDto);

        CollectionItem updated = collectionItemRepository.update(toUpdate);

        return mapper.mapToDto(updated);
    }

    @Override
    public void deleteById(Long id) {

    }
}
