package by.itstep.collections.manager.service;

import by.itstep.collections.manager.dto.collectionDto.CollectionCreateDto;
import by.itstep.collections.manager.dto.collectionDto.CollectionFullDto;
import by.itstep.collections.manager.dto.collectionDto.CollectionPreviewDto;
import by.itstep.collections.manager.dto.collectionDto.CollectionUpdateDto;
import by.itstep.collections.manager.entity.Collection;
import by.itstep.collections.manager.entity.User;
import by.itstep.collections.manager.mapper.CollectionMapper;
import by.itstep.collections.manager.repository.CollectionRepository;
import by.itstep.collections.manager.repository.CollectionRepositoryImpl;
import by.itstep.collections.manager.repository.UserRepository;
import by.itstep.collections.manager.repository.UserRepositoryImpl;

import java.util.List;

public class CollectionServiceImpl implements CollectionService {

    private CollectionRepository collectionRepository = new CollectionRepositoryImpl();
    private UserRepository userRepository = new UserRepositoryImpl();
    private CollectionMapper mapper = new CollectionMapper();

    @Override
    public List<CollectionPreviewDto> findAll() {
        List<Collection> found = collectionRepository.findAll();
        System.out.println("CollectionServiceImpl -> found " + found.size() + " collection");
        return mapper.mapToDtoList(found);
    }

    @Override
    public CollectionFullDto findById(Long id) {
        Collection found = collectionRepository.findById(id);
        System.out.println("CollectionServiceImp -> found: " + found);

        CollectionFullDto responce = mapper.mapToDto(found);

        return responce;
    }

    @Override
    public CollectionFullDto create(CollectionCreateDto createDto) {
        User user = userRepository.findById(createDto.getUserId());
        Collection toSave = mapper.mapToEntity(createDto, user);

        Collection created = collectionRepository.create(toSave);
        System.out.println("CollectionServiceImpl -> created collection: " + created);

        CollectionFullDto responce = mapper.mapToDto(created);

        return responce;
    }

    @Override
    public CollectionFullDto update(CollectionUpdateDto updateDto) {
        Collection toUpdate = mapper.mapToEntity(updateDto);
        Collection existingEntity = collectionRepository.findById(updateDto.getId());

        toUpdate.setImageUrl(existingEntity.getImageUrl());
        toUpdate.setUser(existingEntity.getUser());

        Collection created = collectionRepository.update(toUpdate);
        System.out.println("CollectionServiceImpl -> updated collection: " + created);

        CollectionFullDto responce = mapper.mapToDto(created);
        return responce;
    }

    @Override
    public void deleteById(Long id) {
        collectionRepository.deleteById(id);
        System.out.println("CollectionServiceImpl -> collection with id " + id + " was deleted");
    }
}
