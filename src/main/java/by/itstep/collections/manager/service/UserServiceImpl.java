package by.itstep.collections.manager.service;

import by.itstep.collections.manager.dto.userDto.UserCreateDto;
import by.itstep.collections.manager.dto.userDto.UserFullDto;
import by.itstep.collections.manager.dto.userDto.UserPreviewDto;
import by.itstep.collections.manager.dto.userDto.UserUpdateDto;
import by.itstep.collections.manager.entity.User;
import by.itstep.collections.manager.mapper.UserMapper;
import by.itstep.collections.manager.repository.UserRepository;
import by.itstep.collections.manager.repository.UserRepositoryImpl;

import java.util.List;

public class UserServiceImpl implements UserService {

    private UserRepository userRepository = new UserRepositoryImpl();

    private UserMapper mapper = new UserMapper();

    @Override
    public List<UserPreviewDto> findAll() {
        List<User> found = userRepository.findAll();
        return mapper.mapToDtoList(found);
    }

    @Override
    public UserFullDto findById(Long id) {
        User found = userRepository.findById(id);

        return mapper.mapToDto(found);
    }

    @Override
    public UserFullDto create(UserCreateDto createDto) {
        User toSave = mapper.mapToEntity(createDto);

        User created = userRepository.create(toSave);

        return mapper.mapToDto(created);
    }

    @Override
    public UserFullDto update(UserUpdateDto updateDto) {
        User toUpdate = mapper.mapToEntity(updateDto);

        User existingEntity = userRepository.findById(updateDto.getId());

        toUpdate.setRole(existingEntity.getRole());

        User updated = userRepository.update(toUpdate);

        return mapper.mapToDto(updated);
    }

    @Override
    public void deleteById(Long id) {
        userRepository.deleteById(id);
    }
}
