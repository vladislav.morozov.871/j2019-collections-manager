package by.itstep.collections.manager.service;

import by.itstep.collections.manager.dto.userDto.UserCreateDto;
import by.itstep.collections.manager.dto.userDto.UserFullDto;
import by.itstep.collections.manager.dto.userDto.UserPreviewDto;
import by.itstep.collections.manager.dto.userDto.UserUpdateDto;
import by.itstep.collections.manager.entity.User;

import java.util.List;

public interface UserService {

    List<UserPreviewDto> findAll();

    UserFullDto findById(Long id);

    UserFullDto create(UserCreateDto createDto);

    UserFullDto update(UserUpdateDto updateDto);

    void deleteById(Long id);

}
