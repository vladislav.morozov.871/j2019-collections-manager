package by.itstep.collections.manager.service;

import by.itstep.collections.manager.dto.collectionDto.CollectionCreateDto;
import by.itstep.collections.manager.dto.collectionDto.CollectionFullDto;
import by.itstep.collections.manager.dto.collectionDto.CollectionPreviewDto;
import by.itstep.collections.manager.dto.collectionDto.CollectionUpdateDto;
import by.itstep.collections.manager.dto.collectionItemDto.CollectionItemCreateDto;
import by.itstep.collections.manager.dto.collectionItemDto.CollectionItemFullDto;
import by.itstep.collections.manager.dto.collectionItemDto.CollectionItemPreviewDto;
import by.itstep.collections.manager.dto.collectionItemDto.CollectionItemUpdateDto;
import by.itstep.collections.manager.entity.CollectionItem;

import java.util.List;

public interface CollectionItemService {


    List<CollectionItemPreviewDto> findAll();


    CollectionItemFullDto findById(Long id);


    CollectionItemFullDto create(CollectionItemCreateDto createDto);


    CollectionItemFullDto update(CollectionItemUpdateDto updateDto);


    void deleteById(Long id);

}
