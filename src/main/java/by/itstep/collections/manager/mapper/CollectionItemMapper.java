package by.itstep.collections.manager.mapper;

import by.itstep.collections.manager.dto.collectionItemDto.CollectionItemCreateDto;
import by.itstep.collections.manager.dto.collectionItemDto.CollectionItemFullDto;
import by.itstep.collections.manager.dto.collectionItemDto.CollectionItemPreviewDto;
import by.itstep.collections.manager.dto.collectionItemDto.CollectionItemUpdateDto;
import by.itstep.collections.manager.entity.CollectionItem;

import java.util.ArrayList;
import java.util.List;

public class CollectionItemMapper {

    public List<CollectionItemPreviewDto> mapToDoList(List<CollectionItem> entities) {
        List<CollectionItemPreviewDto> dtos = new ArrayList<>();

        for (CollectionItem entity : entities) {
            CollectionItemPreviewDto dto = new CollectionItemPreviewDto();

            dto.setId(entity.getId());
            dto.setName(entity.getName());

            dtos.add(dto);
        }
        return dtos;
    }

    public CollectionItem mapToEntity(CollectionItemCreateDto createDto) {
        CollectionItem collectionItem = new CollectionItem();
        collectionItem.setName(createDto.getName());

        return collectionItem;
    }

    public CollectionItem mapToEntity(CollectionItemUpdateDto updateDto) {
        CollectionItem collectionItem = new CollectionItem();
        collectionItem.setId(updateDto.getId());
        collectionItem.setName(updateDto.getName());

        return collectionItem;
    }

    public CollectionItemFullDto mapToDto(CollectionItem entity) {
        CollectionItemFullDto fullDto = new CollectionItemFullDto();
        fullDto.setId(entity.getId());
        fullDto.setName(entity.getName());
        fullDto.setCollection(entity.getCollection());

        return fullDto;
    }

}
