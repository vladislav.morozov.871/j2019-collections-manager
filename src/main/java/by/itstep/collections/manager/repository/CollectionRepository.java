package by.itstep.collections.manager.repository;

import by.itstep.collections.manager.entity.Collection;

import java.util.List;

public interface CollectionRepository {

    // 1. Найти все
    List<Collection> findAll();

    // 2. Найти по ID
    Collection findById(Long id);

    // 3. Сохранить новый
    Collection create(Collection collection);

    // 4. Обновить существующий
    Collection update(Collection collection);

    // 5. Удалить по ID
    void deleteById(Long id);

    void deleteAll();


}
