package by.itstep.collections.manager.repository;

import by.itstep.collections.manager.entity.Collection;
import by.itstep.collections.manager.entity.CollectionItem;

import java.util.List;

public interface CollectionItemRepository {

    List<CollectionItem> findAll();

    CollectionItem findById(Long id);

    CollectionItem create(CollectionItem collectionItem);

    CollectionItem update(CollectionItem collectionItem);

    void deleteById(Long id);

    void deleteAll();

}
