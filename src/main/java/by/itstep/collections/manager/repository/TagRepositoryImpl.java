package by.itstep.collections.manager.repository;

import by.itstep.collections.manager.entity.CollectionItem;
import by.itstep.collections.manager.entity.Comment;
import by.itstep.collections.manager.entity.Tag;
import by.itstep.collections.manager.util.EntityManagerUtils;

import javax.persistence.EntityManager;
import java.util.List;

import static by.itstep.collections.manager.util.EntityManagerUtils.getEntityManager;

public class TagRepositoryImpl implements TagRepository {
    @Override
    public List<Tag> findAll() {
        EntityManager em = getEntityManager();

        List<Tag> foundList = em.createNativeQuery("SELECT * FROM tag", Tag.class)
                .getResultList();

        em.close();
        System.out.println("Found " + foundList.size() + " tags");
        return foundList;
    }

    @Override
    public Tag findById(Long id) {
        EntityManager em = getEntityManager();

        Tag foundTag = em.find(Tag.class, id);

        em.close();
        System.out.println("Found tag: " + foundTag);
        return foundTag;
    }

    @Override
    public Tag create(Tag tag) {
        EntityManager em = getEntityManager();
        em.getTransaction().begin();

        em.persist(tag);

        em.getTransaction().commit();
        em.close();
        System.out.println("Tag was created. Id: " + tag.getId());
        return tag;
    }

    @Override
    public Tag update(Tag tag) {
        EntityManager em = getEntityManager();
        em.getTransaction().begin();

        em.merge(tag);

        em.getTransaction().commit();
        em.close();
        System.out.println("Tag was updated. Id: " + tag.getId());
        return tag;
    }

    @Override
    public void deleteById(Long id) {
        EntityManager em = EntityManagerUtils.getEntityManager();
        em.getTransaction().begin();

        Tag foundTag = em.find(Tag.class, id);
        em.remove(foundTag);

        em.getTransaction().commit();
        em.close();
    }
}
