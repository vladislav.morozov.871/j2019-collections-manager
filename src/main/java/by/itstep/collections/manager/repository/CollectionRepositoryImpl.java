package by.itstep.collections.manager.repository;

import by.itstep.collections.manager.entity.Collection;
import by.itstep.collections.manager.util.EntityManagerUtils;
import org.hibernate.Hibernate;

import javax.persistence.EntityManager;
import java.util.List;

import static by.itstep.collections.manager.util.EntityManagerUtils.*;
import static by.itstep.collections.manager.util.EntityManagerUtils.getEntityManager;

public class CollectionRepositoryImpl implements CollectionRepository {

    @Override
    public List<Collection> findAll() {
        EntityManager em = getEntityManager();

        List<Collection> foundList = em.createNativeQuery("SELECT * FROM collection", Collection.class)
                .getResultList();

        em.close();
        System.out.println("Found " + foundList.size() + " collections");
        return foundList;
    }

    @Override
    public Collection findById(Long id) {
        // 1. Получить экземпляр EntityManager'a
        EntityManager em = getEntityManager();

        // 2. Сделать что вам нужно
        Collection foundCollection = em.find(Collection.class, id);
        // foundCollection.getItems().size();
        Hibernate.initialize(foundCollection.getItems());

        // 3. Закрыть EntityManager'a
        em.close();
        System.out.println("Found collection: " + foundCollection);
        return foundCollection;
    }

    @Override
    public Collection create(Collection collection) {
        EntityManager em = getEntityManager();
        em.getTransaction().begin(); // стартуем транзакцию

        em.persist(collection);

        em.getTransaction().commit(); // сохраняем все что есть
        em.close();
        System.out.println("Collection was created. Id: " + collection.getId());
        return collection;
    }

    @Override
    public Collection update(Collection collection) {
        EntityManager em = getEntityManager();
        em.getTransaction().begin(); // стартуем транзакцию

        em.merge(collection);

        em.getTransaction().commit(); // сохраняем все что есть
        em.close();
        System.out.println("Collection was updated. Id: " + collection.getId());
        return collection;
    }

    @Override
    public void deleteById(Long id) {
        EntityManager em = getEntityManager();
        em.getTransaction().begin();

        Collection foundCollection = em.find(Collection.class, id);
        em.remove(foundCollection);

        em.getTransaction().commit();
        em.close();

    }

    @Override
    public void deleteAll() {
        EntityManager em = getEntityManager();
        em.getTransaction().begin();

        em.createNativeQuery("DELETE FROM collection").executeUpdate();

        em.getTransaction().commit();
        em.close();
    }
}
