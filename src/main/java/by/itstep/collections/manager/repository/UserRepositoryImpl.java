package by.itstep.collections.manager.repository;

import by.itstep.collections.manager.entity.Tag;
import by.itstep.collections.manager.entity.User;
import by.itstep.collections.manager.util.EntityManagerUtils;

import javax.persistence.EntityManager;
import java.util.List;

import static by.itstep.collections.manager.util.EntityManagerUtils.getEntityManager;

public class UserRepositoryImpl implements UserRepository {
    @Override
    public List<User> findAll() {
        EntityManager em = getEntityManager();

        List<User> foundList = em.createNativeQuery("SELECT * FROM `user`", User.class)
                .getResultList();

        em.close();
        System.out.println("Found " + foundList.size() + " users");
        return foundList;
    }

    @Override
    public User findById(Long id) {
        EntityManager em = getEntityManager();

        User foundUser = em.find(User.class, id);

        em.close();
        System.out.println("Found user: " + foundUser);
        return foundUser;
    }

    @Override
    public User create(User user) {
        EntityManager em = getEntityManager();
        em.getTransaction().begin();

        em.persist(user);

        em.getTransaction().commit();
        em.close();
        System.out.println("User was created. Id: " + user.getId());
        return user;
    }

    @Override
    public User update(User user) {
        EntityManager em = getEntityManager();
        em.getTransaction().begin();

        em.merge(user);

        em.getTransaction().commit();
        em.close();
        System.out.println("User was updated. Id: " + user.getId());
        return user;
    }

    @Override
    public void deleteById(Long id) {
        EntityManager em = EntityManagerUtils.getEntityManager();
        em.getTransaction().begin();

        User foundUser = em.find(User.class, id);
        em.remove(foundUser);

        em.getTransaction().commit();
        em.close();

    }
}
