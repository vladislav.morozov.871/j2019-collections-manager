package by.itstep.collections.manager.repository;

import by.itstep.collections.manager.entity.Collection;
import by.itstep.collections.manager.entity.CollectionItem;
import by.itstep.collections.manager.util.EntityManagerUtils;

import javax.persistence.EntityManager;
import java.util.List;

import static by.itstep.collections.manager.util.EntityManagerUtils.*;

public class CollectionItemRepositoryImpl implements CollectionItemRepository {
    @Override
    public List<CollectionItem> findAll() {
        EntityManager em = getEntityManager();

        List<CollectionItem> foundList = em.createNativeQuery("SELECT * FROM collection_item", CollectionItem.class)
                .getResultList();

        em.close();
        System.out.println("Found " + foundList.size() + " collectionItems");
        return foundList;
    }

    @Override
    public CollectionItem findById(Long id) {
        EntityManager em = getEntityManager();

        CollectionItem foundCollectionItem = em.find(CollectionItem.class, id);

        em.close();
        System.out.println("Found collectionItem: " + foundCollectionItem);
        return foundCollectionItem;
    }

    @Override
    public CollectionItem create(CollectionItem collectionItem) {
        EntityManager em = getEntityManager();
        em.getTransaction().begin();

        em.persist(collectionItem);

        em.getTransaction().commit();
        em.close();
        System.out.println("CollectionItem was created. Id: " + collectionItem.getId());
        return collectionItem;
    }

    @Override
    public CollectionItem update(CollectionItem collectionItem) {
        EntityManager em = getEntityManager();
        em.getTransaction().begin();

        em.merge(collectionItem);

        em.getTransaction().commit();
        em.close();
        System.out.println("CollectionItem was updated. Id: " + collectionItem.getId());
        return collectionItem;
    }

    @Override
    public void deleteById(Long id) {
        EntityManager em = EntityManagerUtils.getEntityManager();
        em.getTransaction().begin();

        CollectionItem foundCollectionItem = em.find(CollectionItem.class, id);
        em.remove(foundCollectionItem);

        em.getTransaction().commit();
        em.close();
    }

    @Override
    public void deleteAll() {
        EntityManager em = getEntityManager();
        em.getTransaction().begin();

        em.createNativeQuery("DELETE FROM collection_item").executeUpdate();

        em.getTransaction().commit();
        em.close();
    }
}
