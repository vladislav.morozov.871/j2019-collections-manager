package by.itstep.collections.manager.repository;

import by.itstep.collections.manager.entity.CollectionItem;
import by.itstep.collections.manager.entity.Comment;
import by.itstep.collections.manager.util.EntityManagerUtils;

import javax.persistence.EntityManager;
import java.util.List;

import static by.itstep.collections.manager.util.EntityManagerUtils.getEntityManager;

public class CommentRepositoryImpl implements CommentRepository {
    @Override
    public List<Comment> findAll() {
        EntityManager em = getEntityManager();

        List<Comment> foundList = em.createNativeQuery("SELECT * FROM comment", Comment.class)
                .getResultList();

        em.close();
        System.out.println("Found " + foundList.size() + " comments");
        return foundList;
    }

    @Override
    public Comment findById(Long id) {
        EntityManager em = getEntityManager();

        Comment foundComment = em.find(Comment.class, id);

        em.close();
        System.out.println("Found comment: " + foundComment);
        return foundComment;
    }

    @Override
    public Comment create(Comment comment) {
        EntityManager em = getEntityManager();
        em.getTransaction().begin();

        em.persist(comment);

        em.getTransaction().commit();
        em.close();
        System.out.println("Comment was created. Id: " + comment.getId());
        return comment;
    }

    @Override
    public Comment update(Comment comment) {
        EntityManager em = getEntityManager();
        em.getTransaction().begin();

        em.merge(comment);

        em.getTransaction().commit();
        em.close();
        System.out.println("Comment was updated. Id: " + comment.getId());
        return comment;
    }

    @Override
    public void deleteById(Long id) {
        EntityManager em = EntityManagerUtils.getEntityManager();
        em.getTransaction().begin();

        Comment foundComment = em.find(Comment.class, id);
        em.remove(foundComment);

        em.getTransaction().commit();
        em.close();
    }
}
