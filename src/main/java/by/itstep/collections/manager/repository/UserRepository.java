package by.itstep.collections.manager.repository;

import by.itstep.collections.manager.entity.Tag;
import by.itstep.collections.manager.entity.User;

import java.util.List;

public interface UserRepository {

    List<User> findAll();

    User findById(Long id);

    User create(User user);

    User update(User user);

    void deleteById(Long id);
    
}
