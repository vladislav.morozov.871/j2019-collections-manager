package by.itstep.collections.manager.dto.collectionItemDto;

import lombok.Data;

@Data
public class CollectionItemCreateDto {

    private String name;

}
