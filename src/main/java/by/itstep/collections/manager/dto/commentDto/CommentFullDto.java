package by.itstep.collections.manager.dto.commentDto;

import by.itstep.collections.manager.entity.Collection;
import by.itstep.collections.manager.entity.User;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import java.util.Date;

public class CommentFullDto {

    private Long id;

    private String message;

    private User user;

    private Collection collection;

    private Date createdAt;

}
