package by.itstep.collections.manager.dto.userDto;

import lombok.Data;

@Data
public class UserPreviewDto {

    private Long id;

    private String name;

    private String lastName;

    private String email;

}
