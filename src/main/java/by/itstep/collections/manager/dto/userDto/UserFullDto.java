package by.itstep.collections.manager.dto.userDto;

import by.itstep.collections.manager.entity.Collection;
import by.itstep.collections.manager.entity.Comment;
import by.itstep.collections.manager.entity.enums.Role;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.OneToMany;
import java.util.List;

@Data
public class UserFullDto {

    private Long id;

    private String name;

    private String lastName;

    private String email;

    private List<Collection> collections;

    private List<Comment> comments;

    private Role role;

}
