package by.itstep.collections.manager.dto.collectionItemDto;

import lombok.Data;

@Data
public class CollectionItemUpdateDto {

    private Long id;

    private String name;

}
