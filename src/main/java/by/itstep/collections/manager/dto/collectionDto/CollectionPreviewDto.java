package by.itstep.collections.manager.dto.collectionDto;

import by.itstep.collections.manager.entity.Tag;
import lombok.Data;

import java.util.List;

@Data
public class CollectionPreviewDto {

    private Long id;

    private String name;

    private String title;

    private String imageUrl;

    private String userName;

    private List<Tag> tags;

}
