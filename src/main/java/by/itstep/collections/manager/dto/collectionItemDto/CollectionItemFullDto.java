package by.itstep.collections.manager.dto.collectionItemDto;

import by.itstep.collections.manager.entity.Collection;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.persistence.*;

@Data
public class CollectionItemFullDto {

    private Long id;

    private String name;

    private Collection collection;

}
