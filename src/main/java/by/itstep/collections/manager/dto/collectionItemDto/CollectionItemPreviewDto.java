package by.itstep.collections.manager.dto.collectionItemDto;

import lombok.Data;

@Data
public class CollectionItemPreviewDto {

    private Long id;

    private String name;
}
